# eduPrinter

Bash script for installing eduPrint in Linux. Based on the script created by Steffan Emrén available in https://mp.uu.se/en/web/info/stod/it-telefoni/it-servicedesk/utskrift/kom-igang-med-eduprint/eduprint-linux.

## Specifics

Before printing to eduPrint with your Linux computer, you need to install the eduPrint printer. Follow the instructions below to do this.

Your computer must be connected to Uppsala University's wired network or eduroam when installing and printing with eduPrint.

Support for other Linux distributions (Arch and Manjaro) has been added together with an interactive promt to include users' credentials during installation.

## Usage

Open the terminal and clone  this repo.

```git clone https://gitlab.com/pablillocea/eduprinter.git```

Now cd into the folder.

```cd eduprinter```

Give the script execute permision.

```chmod +x install-eduprint.sh```

Run the script and follow the instructions.

```sudo ./install-eduprint.sh```

Done. The printer should work.
